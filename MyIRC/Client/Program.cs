﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Client
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			if (args.Length >= 2 || args.Length <= 5)
			{
				try
				{
					IPAddress address = IPAddress.Parse(args[0]);
					IPEndPoint ipe = new IPEndPoint(address, Convert.ToInt32(args[1]));

					Socket mySocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

					string usr = "";

					if ((args.Length == 5 || args.Length == 4) && args[2] == "-p")
					{
						usr = args[3];
					}
					else
					{
						usr = "root";
					}

					mySocket.Connect(ipe);

					if (mySocket.Connected)
					{
						Console.WriteLine("Connected to " + IPAddress.Parse(((IPEndPoint)mySocket.RemoteEndPoint).Address.ToString()) + ":" + ((IPEndPoint)mySocket.RemoteEndPoint).Port.ToString());
						if (args.Length == 2 || (args.Length == 4 && args[2] == "-p"))
						{
							string input;
							byte[] msg;
							while (true)
							{
								Console.Write(">> ");
								input = Console.ReadLine();
								msg = Encoding.UTF8.GetBytes("From " + usr + " at " + DateTime.Now.ToString("HH:mm:ss") + " : " + input);
								mySocket.Send(msg);
							}
						}
						if (args.Length == 3 || args.Length == 5)
						{
							FileInfo myFile = new FileInfo(args[args.Length - 1]);
							long size = myFile.Length;
							if (size > 2000000000)
							{
								Console.WriteLine("File size cannot be over 2GB");
							}
							else
							{
								mySocket.Send(File.ReadAllBytes(args[args.Length - 1]));
								Console.WriteLine("File send successfully !");
							}
						}
					}
					else
					{
						Console.WriteLine("Could not connect");
					}
				}
				catch (FormatException)
				{
					Console.WriteLine("Wrong format of Ip Adress");
				}
				catch (SocketException)
				{
					Console.WriteLine("Could not connect to " + args[0] + ":" + args[1] + " : Is the server running ?");
				}
				catch (Exception)
				{
					Console.WriteLine("Connexion error...");
				}
			}
			else
			{
				Console.WriteLine("Usage: ./Client.exe ip_adress port_number [ -p pseudo ] [ file ]");
			}
		}
	}
}