﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Server
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			if (args.Length == 1 || args.Length == 2)
			{
				IPAddress localAddr = IPAddress.Parse("127.0.0.1");

				IPEndPoint ipe = new IPEndPoint(localAddr, Convert.ToInt32(args[0]));
				Socket mySocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

				try
				{
					mySocket.Bind(ipe);

					// Nombre de connection simultannee que le server peut recevoir
					mySocket.Listen(1);

					byte[] bytes = new byte[2000000000];

					while (true)
					{
						Socket clientSocket = mySocket.Accept();
						Console.WriteLine("Incoming connection from " + IPAddress.Parse(((IPEndPoint)clientSocket.RemoteEndPoint).Address.ToString()));

						if (args.Length == 1)
						{
							while (clientSocket.Connected)
							{
								int len = clientSocket.Receive(bytes);
								if (len == 0)
								{
									Console.WriteLine("The client is disconnected...");
									break;
								}
								else
								{
									Console.WriteLine(Encoding.UTF8.GetString(bytes, 0, len));
								}
							}
						}
						if (args.Length == 2)
						{
							int len = clientSocket.Receive(bytes);
							File.WriteAllBytes(args[1], bytes.Take(len).ToArray());
						}
					}
				}
				catch (SocketException)
				{
					Console.WriteLine("Could not bind 127.0.0.1:" + args[0] + " : Port already in use");
				}
				catch (Exception)
				{
					Console.WriteLine("Connexion error...");
				}
			}
			else
			{
				Console.WriteLine("Usage: ./Server.exe port_number [file]");
			}
		}
	}
}